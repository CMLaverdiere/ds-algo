#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Quick sort algorithm.
// Runtime: O(nlog(n))

void print_order(int* nums, int size) {
  int i;
  for(i=0; i < size-1; i++) {
    printf("%d -> ", nums[i]);
  }
  printf("%d\n", nums[size-1]);
}

void swap(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}

// Using random pivot.
void quick_sort(int* a, int size) {
  int i, store;
  int pivot_idx, pivot_val;

  if(size < 2) return;

  pivot_idx = rand() % size;
  pivot_val = a[pivot_idx];

  swap(&a[pivot_idx], &a[size-1]);

  for(i=0, store=0; i<size-1; i++) {
    if(a[i] < pivot_val) {
      swap(&a[i], &a[store]);
      store++;
    }
  }

  swap(&a[size-1], &a[store]);

  quick_sort(a, store);
  quick_sort(a+store+1, size-store-1);
}

int main(int argc, const char *argv[])
{
  int i, size;
  int* nums;

  if(argc > 1) {
    size = atoi(argv[1]);
  } else {
    size = 10;
  }

  nums = malloc(sizeof(int) * size);

  srand(time(NULL));

  printf("generating random array of %d numbers... \n", size);
  for(i=0; i<size; i++) {
    nums[i] = rand() % size;
  }

  printf("Unsorted: ");
  print_order(nums, size);

  printf("sorting array... \n");
  quick_sort(nums, size);
  printf("done.\n");

  printf("Sorted: ");
  print_order(nums, size);

  return 0;
}
