#include <stdio.h>
#include <string.h>

// Merge sort algorithm.
// Runtime: O(nlog(n))

void print_order(int* nums, int size) {
  int i;
  for(i=0; i < size-1; i++) {
    printf("%d -> ", nums[i]);
  }
  printf("%d\n", nums[size-1]);
}

void swap(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}

void merge_order(int* nums, int size) {
  int i, j, k;
  int mid = size / 2;
  int mod = size % 2;
  int cpy[size];

  for(i=0, j=0, k=0; i<size && j<mid && k<mid+mod; i++) {
    cpy[i] = nums[j] < nums[mid+k] ? nums[j++] : nums[mid+(k++)];
  }

  while(j < mid) cpy[i++] = nums[j++];
  while(k < mid+mod) cpy[i++] = nums[mid+(k++)];

  memcpy(nums, cpy, size*sizeof(int));
}

void merge_sort(int* nums, int size) {
  if (size < 2) return;
  else if (size == 2) {
    if (nums[0] > nums[1]) {
      swap(&nums[0], &nums[1]);
    }
  }
  else {
    int l = size / 2;
    int r = (size / 2) + (size % 2);

    merge_sort(nums, l);
    merge_sort(nums + l, r);
    merge_order(nums, size);
  }
}

int main(int argc, const char *argv[])
{
  int i;
  int nums[] = {7,3,5,6,4,8,1,0,9,2};
  int size = sizeof(nums) / sizeof(int);

  printf("Unsorted: ");
  print_order(nums, size);

  merge_sort(nums, size);

  printf("Sorted: ");
  print_order(nums, size);

  return 0;
}
