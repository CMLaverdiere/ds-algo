#include <stdio.h>

// Selection sort algorithm.
// Runtime: O(n^2)

int main(int argc, const char *argv[])
{
  int i = 0;
  int j = 0;
  int nums[] = {4, 9, 1, 3, 8, 7};
  int size = sizeof(nums) / 4;

  printf("Unsorted: ");
  for(i=0; i < size; i++) {
    printf("%d ", nums[i]);
  }

  for(i=0; i < size; i++) {
    int smallest = i;
    for(j=i; j < size; j++) {
      if(nums[j] < nums[smallest]) {
        smallest = j;
      }
    }
    int temp = nums[smallest];
    nums[smallest] = nums[i];
    nums[i] = temp;
  }

  printf("\nSorted: ");
  for(i=0; i < size; i++) {
    printf("%d ", nums[i]);
  }

  return 0;
}
