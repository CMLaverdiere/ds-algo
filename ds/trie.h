struct trie {
  int data;
  char key;
  struct trie *children;
  struct trie *sibling;
};

// Core
void init_trie(struct trie *tn, char key, int data);
struct trie* insert_trie(struct trie *tr, char key, int val);
void insert_str_trie(struct trie *tr, char* str, int val);
struct trie* in_trie(struct trie *tr, char* key);

// Helpers
struct trie* in_branch(struct trie *tn, char key);

// Debug
void print_trie(struct trie *tn);
void print_full_trie(struct trie *tn, int level);
