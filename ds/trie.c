#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "trie.h"

// A trie using chars as keys, ints as data.
// Never written one of these before.

// NB: 0 data in a trie indicates it's null.
//     1 data in a trie indicates this isn't a string end node.

// TODO possibly make a default initialization.
void init_trie(struct trie *tr, char key, int data) {
  tr->data = data;
  tr->key = key;
  tr->children = 0;
  tr->sibling = 0;
}

void delete_trie(struct trie *tr) {
  if(tr->children) {
    delete_trie(tr->children);
  }

  if(tr->sibling) {
    delete_trie(tr->sibling);
  }

  free(tr);
}

// Inserting at a trie creates a new sibling trie at the same level, or in the
// case of insertion into an empty trie, simply initializes it.
// It then allocated an empty child trie as well.
//
// ie: at a node 't' with keys {a, b, c}, inserting 'd' at node 't' would make
// the sibling list {a, b, c, d}.
struct trie* insert_trie(struct trie *tr, char key, int val) {
  struct trie *in_trie, *new_child, *temp_sib;

  // In the case of an empty trie, just initialize data.
  if(tr->data == 0) {
    init_trie(tr, key, val);
    in_trie = tr;
  } else { // Otherwise, we allocate a new sibling trie.
    in_trie = malloc(sizeof(struct trie));
    init_trie(in_trie, key, val);

    temp_sib = tr->sibling;
    tr->sibling = in_trie;
    in_trie->sibling = temp_sib;
  }

  // Always keep an allocated child node for later insertion.
  new_child = malloc(sizeof(struct trie));
  new_child->data = 0;
  in_trie->children = new_child;

  return in_trie;
}

// Inserts a string into the trie, traversing through repeated characters.
// Does nothing if string is already inside the trie.
// Should return new node too.
void insert_str_trie(struct trie *tr, char* str, int val) {
  struct trie *match_br, *in_trie;

  if(str[0] == '\0') { return; }

  match_br = in_branch(tr, str[0]);
  if(match_br) {
    insert_str_trie(match_br->children, str+1, val);
  } else {
    if(strlen(str) == 1) {
      in_trie = insert_trie(tr, str[0], val);
    } else {
      in_trie = insert_trie(tr, str[0], 1);
      insert_str_trie(in_trie->children, str+1, val);
    }
  }
}

struct trie* in_branch(struct trie *tr, char key) {
  struct trie *current = tr;
  while(current) {
    if(current->key == key) return current;
    current = current->sibling;
  }
  return 0;
}

struct trie* in_trie(struct trie *tr, char* key) {
  struct trie *current, *match;

  current = tr;

  if(!in_branch(tr, key[0])) {
    return 0;
  } else {
    if(strlen(key) == 1) { return current; }
    else {
      while(current) {
        match = in_trie(current->children, key+1);
        if(match) { return match; }

        current = current->sibling;
      }
      return 0;
    }
  }
}

void print_trie(struct trie *tn) {
  if(tn) {
    printf("Tree: data -> %d\n"
           "      key  -> %c\n"
           "      children  -> %p\n"
           "      sibling   -> %p",
           tn->data, tn->key, tn->children, tn->sibling);
  } else {
    printf("Tree: Empty\n");
  }
}

void print_full_trie(struct trie *tn, int level) {
  if(tn) {
    printf("%*sTree | data -> %d |"
                    " key -> %c |"
                    " children -> %p |"
                    " sibling -> %p\n",
           level, "", tn->data, tn->key, tn->children, tn->sibling);
    print_full_trie(tn->children, level+1);
    if(tn->sibling) { print_full_trie(tn->sibling, level); }
  }
}

int main(int argc, const char *argv[])
{
  int TRIE_SIZE = 8;
  int TRIE_DATA_LIM = 256;
  int vals[TRIE_SIZE];

  srand(time(NULL));

  struct trie *tr = malloc(sizeof(struct trie));

  int i;
  char *keys[] = {"to", "tea", "ted", "ten", "inn"};

  for(i=0; i < sizeof(keys) / sizeof(keys[0]); i++) {
    int newval = rand() % TRIE_DATA_LIM;
    char* key = keys[i];
    insert_str_trie(tr, key, newval);
  }

  print_full_trie(tr, 0);

  delete_trie(tr);

  return 0;
}
