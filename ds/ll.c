#include <stdio.h>
#include <stdlib.h>

// Singly Linked list

struct lnode {
  int data;
  struct lnode *next;
};

struct llist {
  struct lnode *start; 
};

void init_lnode(struct lnode *ln, int data) {
  ln->data = data;
  ln->next = 0;
}

void init_ll(struct llist *ll) {
  struct lnode *start_node = malloc(sizeof(struct lnode));
  init_lnode(start_node, 0);
  start_node->next = 0;

  ll->start = start_node;
}

// Insert at front.
void insert_ll(struct llist *ll, int data) {
  struct lnode *ln = malloc(sizeof(struct lnode));
  init_lnode(ln, data);

  ln->next = ll->start;
  ll->start = ln;
}

void print_ll(struct llist *ll) {
  struct lnode* ln = ll->start;
  while(ln) {
    printf("%d -> ", ln->data);
    ln = ln->next;
  }
  printf("#");
}

void delete_lnode(struct lnode *ln) {
  free(ln->next);
}

void delete_ll(struct llist *ll) {
  struct lnode *ln = ll->start, *tmp;
  while(ln) {
    tmp = ln;
    ln = ln->next;
    delete_lnode(tmp);
  }
  free(ll->start);
  free(ll);
}

int main(int argc, const char *argv[])
{
  int i;
  int data[] = {2, 1, 4, 5, 3, 6, 7};
  int size = sizeof(data) / sizeof(int);

  struct llist *ll = malloc(sizeof(struct llist));
  init_ll(ll);

  for(i=0; i<size; i++) {
    insert_ll(ll, data[i]);
  }

  print_ll(ll);

  delete_ll(ll);
  return 0;
}

// Now delete everything.
