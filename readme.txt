Data Structures and Algorithms

This is a collection of things I felt like learning to implement for some
reason. Ranges from simple exercises like LLs and BSTs, to structures that
wouldn't be commonly implemented in a DS or algos university class.
